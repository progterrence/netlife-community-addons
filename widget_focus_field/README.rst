.. image:: https://img.shields.io/badge/licence-LGPL--3-blue.svg
   :target: https://www.gnu.org/licenses/agpl
   :alt: License: LGPL-3

Widget Focus Field
==================
This module makes a field be on focus in X2many field
tree editable mode.

**NB:** Only one field can be focusable.

USAGE
=====
Add *options="{'focus': true}"* on field xml view as shown
below

   .. code-block:: xml

       <field name="lot_id" options="{'focus': true}"/>

