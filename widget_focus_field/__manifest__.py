# Copyright 2021 Sunflower IT (https://www.sunflowerweb.nl)
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).

{
    'name': 'Widget Focus Field',
    'version': '12.0.1.0.1',
    'category': 'web',
    'sequence': 1,
    'summary': 'Makes a field focused',
    'author': 'Sunfower IT',
    'depends': [
        'web',
    ],
    'data': [
        'views/web_assets.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
