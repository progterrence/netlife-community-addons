odoo.define('widget_focus_field.EditableListRenderer', function (require) {
    "use strict";

    var ListRenderer = require('web.ListRenderer');

    ListRenderer.include({
        _moveToNextLine: function () {
            var self = this;
            var record = this.state.data[this.currentRow];
            var focus_col = this.columns.findIndex((x)=> x.attrs.options &&
                JSON.parse(x.attrs.options.replace(/'/g, '"')).focus);
            this.commitChanges(record.id).then(function () {
                var fieldNames = self.canBeSaved(record.id);
                if (fieldNames.length) {
                    return;
                }
                var fieldIndex = focus_col || 0;
                if (self.currentRow < self.state.data.length - 1) {
                    self._selectCell(self.currentRow + 1, fieldIndex);
                } else {
                    self.unselectRow().then(function () {
                        self.trigger_up('add_record', {
                            onFail: self._selectCell.bind(self, 0,
                                fieldIndex, {}),
                        });
                    });
                }
            });
            },
    });
})