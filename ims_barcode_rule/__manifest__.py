# Copyright 2021 Sunflower IT (https://www.sunflowerweb.nl)
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).

{
    'name': 'IMS Barcode Rule',
    'version': '12.0.0.0.1',
    'category': 'Warehouse',
    'sequence': 1,
    'summary': 'IMS create rule for dynamic use',
    'author': 'Sunfower IT',
    'depends': [
        'stock',
        'ims_base'
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/ims_barcode_rule_data.xml',
        'views/ims_barcode_rule.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
}
