.. image:: https://img.shields.io/badge/licence-LGPL--3-blue.svg
   :target: https://www.gnu.org/licenses/agpl
   :alt: License: LGPL-3

IMS Barcode Rule
================
This module allows you to add regex rule(s) to be used in processing of
barcodes.