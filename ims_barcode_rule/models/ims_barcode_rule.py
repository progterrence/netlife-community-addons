# Copyright 2021 Sunflower IT (https://www.sunflowerweb.nl)
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl).

import re

from odoo import models, fields, api


class ImsBarcoderule(models.Model):
    _name = 'ims.barcode.rule'
    _description = "IMS Barcode rule"

    name = fields.Char(
        string='Name',
        readonly=1,
        default=lambda self: self.env['ir.sequence'].next_by_code(
            'ims.barcode.rule') or '')
    description = fields.Char(string="Description")
    active = fields.Boolean(string='active', default=True)
    start_rule = fields.Char(string="Start string")
    part_rule = fields.Char(string="Product rule")
    serial_rule = fields.Char(string="Serial rule")
    rev_rule = fields.Char(string="Ref rule")
    md_rule = fields.Char(
        string="MD Rule",
        help="Manufacturing date rule")
    test_string = fields.Text(string="Test Barcode String")
    result = fields.Html(string="Result")

    @api.onchange('test_string')
    def _onchange_test_result(self):
        if self.test_string:
            code = re.findall('[/0-9a-zA-Z:,]+', self.test_string)
            self.test_string = "".join(code)
        if self.result:
            self.result = ''
        return

    @api.multi
    def action_test_expression(self):
        self.ensure_one()
        result = ""
        code = self.test_string
        if code:
            code = code.replace(" ", "")  # remove whitespace
            if self.part_rule:
                part = re.findall(r'{0}'.format(self.part_rule), code)
                if any(part):
                    result += '<li style=\'color: red;\'> <b>PART</b>: ' + \
                              part[0] + '</li>'
            if self.rev_rule:
                rev = re.findall(r'{0}'.format(self.rev_rule), code)
                if any(rev):
                    result += '<li style=\'color: green;\'> <b>REF</b>: ' + \
                              rev[0] + '</li>'
            if self.serial_rule:
                serial = re.findall(r'{0}'.format(self.serial_rule), code)
                if any(serial):
                    result += '<li style=\'color: purple;\'> <b>SERIAL</b>: '\
                              + serial[0] + '</li>'
            if self.md_rule:
                serial = re.findall(r'{0}'.format(self.md_rule), code)
                if any(serial):
                    result += '<li style=\'color: blue;\'> ' \
                              '<b>MANUFACTURE DATE</b>: '\
                              + serial[0] + '</li>'
            result = '<ul>' + result + '<ul>'
            self.result = result
