# -*- coding: utf-8 -*-
{
    'name': 'Locations Maps',
    'version': '12.0.1.0.4',
    'author': 'Yopi Angi',
    'license': 'AGPL-3',
    'maintainer': 'Yopi Angi<yopiangi@gmail.com>',
    'support': 'yopiangi@gmail.com',
    'category': 'Tools',
    'description': """
Contacts Maps
=============

Added map view on contacts
""",
    'depends': [
        'stock',
        'ims_stock',
        'ims_customer_site',
        'contacts_maps',
        'base_geolocalize',
        'google_marker_icon_picker',
        'ims_base'
    ],
    'website': '',
    'data': [
        'views/stock_location.xml',
        'views/stock_warehouse.xml',
        'views/stock_quant.xml',
        'views/res_partner.xml',
        'wizards/auto_assign_missing_geopoints.xml',
    ],
    'demo': [],
    'installable': True
}
