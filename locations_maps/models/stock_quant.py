# -*- coding: utf-8 -*-
from odoo import fields, models


class StockQuant(models.Model):
    _inherit = 'stock.quant'

    address_id = fields.Many2one('res.partner', related='location_id.address_id')

    marker_color = fields.Char(related='location_id.marker_color')
    location_longitude = fields.Float(related='location_id.location_longitude')
    location_latitude = fields.Float(related='location_id.location_latitude')
    phone = fields.Char(related='location_id.phone')
    mobile = fields.Char(related='location_id.mobile')
    street = fields.Char(related='location_id.street')
    street2 = fields.Char(related='location_id.street2')
    zip = fields.Char(related='location_id.zip')
    city = fields.Char(related='location_id.city')
    country_id = fields.Many2one('res.country', related='location_id.country_id')
    country_state_id = fields.Many2one('res.country.state',
        related='location_id.state_id')
    image_small = fields.Binary(related='location_id.image_small')
