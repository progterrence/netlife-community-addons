# -*- coding: utf-8 -*-
from odoo import fields, models, _, api


class StockWarehouse(models.Model):
    _inherit = 'stock.warehouse'

    marker_color = fields.Char(related='address_id.marker_color')
    location_longitude = fields.Float(related='address_id.partner_longitude')
    location_latitude = fields.Float(related='address_id.partner_latitude')
    phone = fields.Char(related='address_id.phone')
    mobile = fields.Char(related='address_id.mobile')
    street = fields.Char(related='address_id.street')
    street2 = fields.Char(related='address_id.street2')
    zip = fields.Char(related='address_id.zip')
    city = fields.Char(related='address_id.city')
    state_id = fields.Many2one('res.country.state',
                               related='address_id.state_id')
    image_small = fields.Binary(related='address_id.image_small')

    def action_view_all_locations(self):
        self.ensure_one()
        domain = [
            ('id', 'child_of', self.view_location_id.id),
            ('usage', '=', 'internal')
        ]
        return {
            'name': _('Current Locations'),
            'domain': domain,
            'res_model': 'stock.location',
            'view_id': False,
            'view_mode': 'tree,form',
            'type': 'ir.actions.act_window'

        }

    @api.onchange('address_id')
    def _onchange_address_id(self):
        if self.address_id:
            if not self.location_latitude:
                self.location_longitude = self.address_id.partner_longitude
            if not self.location_longitude:
                self.location_latitude = self.address_id.partner_latitude
