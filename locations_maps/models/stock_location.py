# -*- coding: utf-8 -*-
from odoo import fields, models


class StockLocation(models.Model):
    _inherit = 'stock.location'

    address_id = fields.Many2one('res.partner')

    marker_color = fields.Char(related='address_id.marker_color')
    location_longitude = fields.Float(related='address_id.partner_longitude')
    location_latitude = fields.Float(related='address_id.partner_latitude')
    phone = fields.Char(related='address_id.phone')
    mobile = fields.Char(related='address_id.mobile')
    street = fields.Char(related='address_id.street')
    street2 = fields.Char(related='address_id.street2')
    zip = fields.Char(related='address_id.zip')
    city = fields.Char(related='address_id.city')
    country_id = fields.Many2one('res.country', related='address_id.country_id')
    state_id = fields.Many2one('res.country.state',
        related='address_id.state_id')
    image_small = fields.Binary(related='address_id.image_small')
