# -*- coding: utf-8 -*-

from odoo import api, models, _


class AutoAssignGeoPoints(models.TransientModel):
    _name = 'auto.assign.geo.points.wizard'
    _description = 'Auto Assign Missing Geo Points'

    @api.multi
    def action_assign_geopoints(self):
        locations_missing_geopoints = self.env['stock.location'].search([
            '&',
            ('location_longitude', '=', 0.0),
            ('location_latitude', '=', 0.0)
        ])
        default_lat = self.env.user.company_id.partner_id.partner_latitude
        default_long = self.env.user.company_id.partner_id.partner_longitude
        if locations_missing_geopoints:
            locations_missing_geopoints.mapped('address_id').write({
                'partner_longitude': default_long,
                'partner_latitude': default_lat
            })
        return {'type': 'ir.actions.act_window_close'}
