# -*- coding: utf-8 -*-
{
    'name': 'Auth Keycloak Update Password',
    'version': '12.0.1.0.4',
    'author': 'Terrence Nzaywa',
    'license': 'AGPL-3',
    'maintainer': 'Terrence Nzaywa',
    'support': 'nza.terrence@gmail.com',
    'category': 'Tools',
    'depends': ['auth_keycloak', 'base'],
    'website': '',
    'data': [
        # 'views/res_partner.xml',
    ],
    'demo': [],
    'installable': True
}
