# -*- coding: utf-8 -*-
from odoo import fields, models, api, exceptions, _
from odoo.exceptions import UserError
import logging
import requests
try:
    from json.decoder import JSONDecodeError
except ImportError:
    # py2
    JSONDecodeError = ValueError

logger = logging.getLogger(__name__)


class ResUsers(models.Model):
    _inherit = "res.users"

    def _get_keycloak_token(self):
        self.ensure_one()
        """Retrieve auth token from Keycloak."""
        provider_id = self.env.ref('auth_keycloak.default_keycloak_provider')
        url = provider_id.validation_endpoint.replace('/introspect', '')
        headers = {'content-type': 'application/x-www-form-urlencoded'}
        data = {
            'username': provider_id.superuser,
            'password': provider_id.superuser_pwd,
            'grant_type': 'password',
            'client_id': provider_id.client_id,
            'client_secret': provider_id.client_secret,
        }
        resp = requests.post(url, data=data, headers=headers)
        self._validate_response(resp)
        return resp.json()['access_token']

    def _validate_response(self, resp, no_json=False):
        """Make sure Keycloak answered properly."""
        if not resp.ok:
            # TODO: do something better?
            raise resp.raise_for_status()
        if no_json:
            return resp.content
        try:
            return resp.json()
        except JSONDecodeError:
            raise exceptions.UserError(
                _('Something went wrong. Please check logs.')
            )

    def update_user_pwd_keycloak(self, oauth_uid, pwd):
        token = self._get_keycloak_token()
        provider_id = self.env.ref('auth_keycloak.default_keycloak_provider')
        """Update keycloak user pwd"""
        logger.info('Calling %s' % provider_id.users_endpoint)
        update_pwd_url = "%s/%s/reset-password" % (provider_id.users_endpoint, oauth_uid)
        headers = {
            'Authorization': 'Bearer %s' % token,
            'content-type': 'application/json'
        }
        data = {
            "type": "password",
            "temporary": "false",
            "value": pwd
        }
        resp = requests.put(update_pwd_url, json=data, headers=headers)
        return resp

    def enable_disable_user_keycloak(self, oauth_uid, enabled):
        token = self._get_keycloak_token()
        provider_id = self.env.ref('auth_keycloak.default_keycloak_provider')
        """Update keycloak user pwd"""
        logger.info('Calling %s' % provider_id.users_endpoint)
        update_enable_url = "%s/%s" % (provider_id.users_endpoint, oauth_uid)
        headers = {
            'Authorization': 'Bearer %s' % token,
            'content-type': 'application/json'
        }
        data = {
            "enabled": enabled
        }
        resp = requests.put(update_enable_url, json=data, headers=headers)
        return resp

    @api.model
    def signup(self, values, token=None):
        ret = super(ResUsers, self).signup(values, token=token)
        user = self.env['res.users'].sudo().search([('login', '=', ret[1])], limit=1)
        if 'password' in values and user and user.oauth_uid:
            user.update_user_pwd_keycloak(user.oauth_uid, values['password'])
        return ret

    @api.multi
    def write(self, vals):
        ret = super(ResUsers, self).write(vals)
        for user in self:
            if 'active' in vals and user.oauth_uid:
                user.enable_disable_user_keycloak(user.oauth_uid, vals['active'])
        return ret

class ChangePasswordUser(models.TransientModel):
    """ A wizard to manage the change of users' passwords.
    Update Keycloak user passwords"""
    _inherit = "change.password.user"

    @api.multi
    def change_password_button(self):
        for line in self:
            if not line.new_passwd:
                raise UserError(_("Before clicking on 'Change Password', you have to write a new password."))
            line.user_id.write({'password': line.new_passwd})
            if line.user_id.oauth_uid:
                line.user_id.update_user_pwd_keycloak(line.user_id.oauth_uid, line.new_passwd)

        # don't keep temporary passwords in the database longer than necessary
        self.write({'new_passwd': False})

