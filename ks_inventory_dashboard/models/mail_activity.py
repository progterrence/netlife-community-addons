# Copyright 2020 Sunflower IT <http://sunflowerweb.nl>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields, api


class MailActivity(models.Model):
    _inherit = 'mail.activity'

    current_user_id = fields.Many2one(
        'res.users',
        'Logged On',
        domain=lambda self: [('id', '=', self.env.user.id)],
    )
