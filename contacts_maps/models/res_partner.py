# -*- coding: utf-8 -*-
from odoo import fields, models, api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.model
    def _default_lat(self):
        return self.env.user.company_id.partner_id.partner_latitude

    @api.model
    def _default_long(self):
        return self.env.user.company_id.partner_id.partner_longitude

    marker_color = fields.Char(
        string='Marker Color', default='red', required=True)

    partner_latitude = fields.Float(default=_default_lat)
    partner_longitude = fields.Float(default=_default_long)

