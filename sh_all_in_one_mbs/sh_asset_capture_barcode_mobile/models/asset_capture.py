# -*- coding: utf-8 -*-
# Copyright (C) Softhealer Technologies.
import re
from collections import Counter

from odoo import models, fields, api, _
from odoo.exceptions import Warning, UserError


class ImsAssetCapture(models.Model):
    _inherit = "ims.asset.capture"

    def default_sh_asset_capture_is_cont_scan(self):
        if self.env.user and self.env.user.company_id:
            return self.env.user.company_id.sh_asset_capture_is_cont_scan

    sh_asset_capture_barcode_mobile = fields.Char(string="Mobile Barcode")

    sh_asset_capture_is_cont_scan = fields.Char(string='Continuously Scan?',
                                                default=default_sh_asset_capture_is_cont_scan,
                                                readonly=True)

    @api.multi
    def _get_closest_rule(self, code):
        rules = self.env['ims.barcode.rule'].search([('active', '=', True)])
        rule_objs = rules.filtered(lambda x: code.startswith(x.start_rule))
        if rule_objs:
            rule = rule_objs[0]
            return rule
        return False

    @api.multi
    def _get_rule(self, code):
        code = code.replace(" ", "").strip()
        pattern = r'{0}'.format('^[[)>]|(1P)|(21P)|(22P)')
        results = re.findall(pattern, code)
        if any(results):
            res = [x for m in results for x in m if x != '']
            if Counter(res) == Counter(['21P', '22P', '1P']):
                return self.env.ref('ims_barcode_rule.ims_rule_default_case_1')
            elif Counter(res) == Counter(['1P', '22P']):
                return self.env.ref('ims_barcode_rule.ims_rule_default_case_2')
            elif Counter(res) == Counter(['1P', '21P']):
                return self.env.ref('ims_barcode_rule.ims_rule_default_case_3')
            else:
                return self._get_closest_rule(code)
        return self._get_closest_rule(code)

    @api.multi
    def parse_barcode(self, code):
        code = code.replace(" ", "").strip()
        code = re.findall('[/0-9a-zA-Z:,]+', code)  # exclude special characters
        code = "".join(code)
        rule = self._get_rule(code)
        if rule:
            result = {}
            for res in rule:
                if res.part_rule:
                    part = re.findall(r'{0}'.format(res.part_rule), code)
                    if any(part):
                        result.update({'part': part[0]})
                if res.serial_rule:
                    serial = re.findall(r'{0}'.format(res.serial_rule), code)
                    if any(serial):
                        result.update({'serial': serial[0]})
                if res.rev_rule:
                    rev = re.findall(r'{0}'.format(res.rev_rule), code)
                    if any(rev):
                        result.update({'rev': rev[0]})
            if any(result):
                return result
        return False

    @api.onchange('sh_asset_capture_barcode_mobile')
    def _onchange_sh_asset_capture_barcode_mobile(self):
        if self.sh_asset_capture_barcode_mobile in ['', "", False, None]:
            return
        CODE_SOUND_SUCCESS = ""
        CODE_SOUND_FAIL = ""
        code = self.parse_barcode(self.sh_asset_capture_barcode_mobile)
        if self.env.user.company_id.sudo().sh_asset_capture_is_sound_on_success:
            CODE_SOUND_SUCCESS = "SH_BARCODE_MOBILE_SUCCESS_"

        if self.env.user.company_id.sudo().sh_asset_capture_is_sound_on_fail:
            CODE_SOUND_FAIL = "SH_BARCODE_MOBILE_FAIL_"

        # step 1 make sure order in proper state.
        if self and self.state in ["validation", "done"]:
            selections = self.fields_get()["state"]["selection"]
            value = next((v[1] for v in selections if v[0] == self.state),
                         self.state)
            if self.env.user.company_id.sudo()\
                    .sh_asset_capture_is_notify_on_fail:
                message = _(
                    CODE_SOUND_FAIL + 'You can not scan item in %s state.') % (
                              value)

                # self.env.user.notify_warning(message,
                # title=_('Failed'), sticky=False)

            return

        # step 2 increaset asset qty by 1 if asset not in equipment
        # than create new equipment.
        elif self:
            search_lines = False
            domain = []
            if self.env.user.company_id.sudo()\
                    .sh_asset_capture_barcode_mobile_type == "barcode":
                if code and 'part' in code:
                    search_lines = self.equipments.filtered(
                        lambda ol: ol.product_id.default_code == code['part'])
                    domain = [
                        ('default_code', '=', code['part'])
                    ]
                else:
                    search_lines = self.equipments.filtered(
                        lambda ol: ol.product_id.barcode ==
                        self.sh_asset_capture_barcode_mobile)
                    domain = [
                        ("barcode", "=", self.sh_asset_capture_barcode_mobile)]

            elif self.env.user.company_id.sudo()\
                    .sh_asset_capture_barcode_mobile_type == "int_ref":
                if code and 'part' in code:
                    search_lines = self.equipments.filtered(
                        lambda ol: ol.product_id.default_code == code['part'])
                    domain = [
                        ('default_code', '=', code['part'])
                    ]
                else:
                    search_lines = self.equipments.filtered(
                        lambda ol: ol.product_id.default_code ==
                        self.sh_asset_capture_barcode_mobile)
                    domain = [
                        ("default_code", "=",
                         self.sh_asset_capture_barcode_mobile)]

            elif self.env.user.company_id.sudo()\
                    .sh_asset_capture_barcode_mobile_type == "sh_qr_code":
                if code and 'part' in code:
                    search_lines = self.equipments.filtered(
                        lambda ol: ol.product_id.default_code == code['part'])
                    domain = [
                        ('default_code', '=', code['part'])
                    ]
                else:
                    search_lines = self.equipments.filtered(
                        lambda ol: ol.product_id.sh_qr_code ==
                        self.sh_asset_capture_barcode_mobile)
                    domain = [
                        ("sh_qr_code", "=", self.sh_asset_capture_barcode_mobile
                         )]

            elif self.env.user.company_id.sudo()\
                    .sh_asset_capture_barcode_mobile_type == "all":
                if code:
                    lot = 0
                    if 'serial' in code:
                        lote = self.env["stock.production.lot"].search(
                            [('name', '=', code['serial'])])
                    if lote:
                        prd = lote.product_id.id
                    search_lines = self.equipments.filtered(
                        lambda ol: ol.product_id.barcode ==
                        self.sh_asset_capture_barcode_mobile or
                        ol.product_id.default_code == code['part'] or
                        ol.product_id.sh_qr_code ==
                        self.sh_asset_capture_barcode_mobile or
                        ol.product_id.id == prd)

                    domain = ["|", "|", "|",
                              ("default_code", "=", code['part']),
                              ("barcode", "=", self.sh_asset_capture_barcode_mobile
                               ),
                              ("sh_qr_code", "=",
                               self.sh_asset_capture_barcode_mobile),
                              ("id", "=", prd),
                              ]
                else:
                    lot = 0
                    lote = self.env["stock.production.lot"].search(
                        [('name', '=', self.sh_asset_capture_barcode_mobile)])
                    if lote:
                        lot = lote.product_id.id
                    search_lines = self.equipments.filtered(
                        lambda ol: ol.product_id.barcode ==
                        self.sh_asset_capture_barcode_mobile or
                        ol.product_id.default_code ==
                        self.sh_asset_capture_barcode_mobile or
                        ol.product_id.sh_qr_code ==
                        self.sh_asset_capture_barcode_mobile or
                        ol.product_id.id == lot)

                    domain = ["|", "|", "|",
                              ("default_code", "=",
                               self.sh_asset_capture_barcode_mobile),
                              ("barcode", "=",
                               self.sh_asset_capture_barcode_mobile
                               ),
                              ("sh_qr_code", "=",
                               self.sh_asset_capture_barcode_mobile),
                              ("id", "=", lot),
                              ]
            if search_lines:
                for line in search_lines:
                    line.product_uom_qty += 1

                    if self.env.user.company_id.sudo()\
                            .sh_asset_capture_is_notify_on_success:
                        message = _(
                            CODE_SOUND_SUCCESS + 'Product: %s Qty: %s') % (
                                      line.product_id.name,
                                      line.product_uom_qty)
                        self.env.user.notify_info(message, title=_('Succeed'),
                                                  sticky=False)
                        self.sh_asset_capture_barcode_mobile = ''
                    break

            else:
                search_product = self.env["product.product"].search(domain,
                                                                    limit=1)
                if search_product:
                    lot_id = False
                    vals = {
                        'product_id': search_product.id,
                        'product_uom_qty': 1
                    }
                    if code:
                        if 'serial' in code:
                            lot_id = self.env['stock.production.lot'].search([
                                ('product_id', '=', search_product.id),
                                ('name', '=', code['serial'])
                            ])
                            if lot_id:
                                vals.update({'lot_id': lot_id.id})
                    new_order_line = self.equipments.new(vals)
                    self.equipments += new_order_line
                    if self.env.user.company_id.sudo()\
                            .sh_asset_capture_is_notify_on_success:
                        message = _(
                            CODE_SOUND_SUCCESS + 'Product: %s Qty: %s') % (
                                      new_order_line.product_id.name,
                                      new_order_line.product_uom_qty)
                        self.env.user.notify_info(message, title=_('Succeed'),
                                                  sticky=False)
                        self.sh_asset_capture_barcode_mobile = ''
                else:
                    if self.env.user.company_id.sudo()\
                            .sh_asset_capture_is_notify_on_fail:
                        message = _(CODE_SOUND_FAIL +
                        'Scanned Internal Reference/Barcode not exist in '
                        'any product!')
                        # self.env.user.notify_warning(message, title=
                        # _('Failed'), sticky=False)
                    self.sh_asset_capture_barcode_mobile = ''
