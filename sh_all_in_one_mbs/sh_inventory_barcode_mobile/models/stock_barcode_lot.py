# Copyright (C) Softhealer Technologies.

from odoo import models, fields


class StockBarcodeLot(models.Model):
    _name = "stock.barcode.lot"
    _description = "Stock Barcode Lot"

    name = fields.Char(related='product_id.name')
    barcode = fields.Char(string='Barcode')
    product_id = fields.Many2one('product.product', string="Product")
    lot_id = fields.Many2one('stock.production.lot', string="Serial No")
    move_id = fields.Many2one('stock.move', string="Stock Move")

