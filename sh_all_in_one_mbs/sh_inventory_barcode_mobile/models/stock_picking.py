# -*- coding: utf-8 -*-
# Copyright (C) Softhealer Technologies.

import re
from collections import Counter

from odoo import models, fields, api, _
from odoo.exceptions import Warning, UserError
import logging

from collections import OrderedDict

_logger = logging.getLogger(__name__)

class StockPicking(models.Model):
    _inherit = "stock.picking"

    def default_sh_stock_bm_is_cont_scan(self):
        if self.env.user and self.env.user.company_id:
            return self.env.user.company_id.sh_stock_bm_is_cont_scan

    sh_stock_barcode_mobile = fields.Char(string="Item Barcode")
    sh_stock_barcode_mobile_serial = fields.Char(string="Serial Barcode")
    sh_stock_barcode_message = fields.Char()
    sh_stock_barcode_message_serial = fields.Char()
    sh_stock_bm_is_cont_scan = fields.Char(
        string='Continuously Scan?',
        default=default_sh_stock_bm_is_cont_scan,
        readonly=True)

    @api.multi
    def _get_closest_rule(self, code):
        rules = self.env['ims.barcode.rule'].search([('active', '=', True)])
        rule_objs = rules.filtered(lambda x: code.startswith(x.start_rule))
        if rule_objs:
            rule = rule_objs[0]
            return rule
        return False

    @api.multi
    def _get_rule(self, code):
        code = code.replace(" ", "").strip()
        pattern = r'{0}'.format('^[[)>]|(1P)|(21P)|(22P)')
        results = re.findall(pattern, code)
        if any(results):
            res = [x for m in results for x in m if x != '']
            if Counter(res) == Counter(['21P', '22P', '1P']):
                return self.env.ref('ims_barcode_rule.ims_rule_default_case_1')
            elif Counter(res) == Counter(['1P', '22P']):
                return self.env.ref('ims_barcode_rule.ims_rule_default_case_2')
            elif Counter(res) == Counter(['1P', '21P']):
                return self.env.ref('ims_barcode_rule.ims_rule_default_case_3')
            else:
                return self._get_closest_rule(code)
        return self._get_closest_rule(code)

    @api.multi
    def parse_barcode(self, code):
        code = code.replace(" ", "").strip()
        code = re.findall('[/0-9a-zA-Z:,]+', code)  # exclude special characters
        code = "".join(code)
        rule = self._get_rule(code)
        if rule:
            result = {}
            for res in rule:
                if res.part_rule:
                    part = re.findall(r'{0}'.format(res.part_rule), code)
                    if any(part):
                        result.update({'part': part[0]})
                if res.serial_rule:
                    serial = re.findall(r'{0}'.format(res.serial_rule), code)
                    if any(serial):
                        result.update({'serial': serial[0]})
                if res.rev_rule:
                    rev = re.findall(r'{0}'.format(res.rev_rule), code)
                    if any(rev):
                        result.update({'rev': rev[0]})
            if any(result):
                return result
        return False

    @api.onchange('sh_stock_barcode_mobile')
    def _onchange_sh_stock_barcode_mobile(self):
        self.sh_stock_barcode_message = False
        if self.sh_stock_barcode_mobile in ['', "", False, None]:
            return
        if not self.picking_type_id and not self.lifecycle_stage:
            raise UserError('Fill the required fields of the order first.')
        if self:
            # get the break down of the barcode into 3 parts ['part no',
            # 'rev', 'serial']
            code = self.parse_barcode(self.sh_stock_barcode_mobile)
            search_mls = False
            domain = []
            if code is not False and len(code) > 1:
                if 'part' in code:
                    search_mls = self.move_ids_without_package.filtered(
                        lambda ml: ml.product_id.default_code == code['part'])
                    domain = [
                        ('default_code', '=', code['part'])
                    ]

            elif code is not False and len(code) <= 1:
                if 'part' in code:
                    search_mls = self.move_ids_without_package.filtered(
                        lambda ml: ml.product_id.default_code == code['part'])
                    domain = [('default_code', '=', code['part'])]
            else:
                if self.env.user.company_id.sudo().sh_stock_barcode_mobile_type \
                        == 'barcode':
                    search_mls = self.move_ids_without_package.filtered(
                        lambda ml: ml.product_id.barcode ==
                                   self.sh_stock_barcode_mobile)
                    domain = [("barcode", "=", self.sh_stock_barcode_mobile)]
                if self.env.user.company_id.sudo().sh_stock_barcode_mobile_type \
                        == 'int_ref':
                    search_mls = self.move_ids_without_package.filtered(
                        lambda ml: ml.product_id.default_code ==
                                   self.sh_stock_barcode_mobile)
                    domain = [
                        ("default_code", "=", self.sh_stock_barcode_mobile)]
            search_product = self.env["product.product"].search(domain, limit=1)
            if search_mls:
                for move_line in search_mls:
                    if self.state == 'draft':
                        move_line.product_uom_qty += 1
                    else:
                        move_line.quantity_done = move_line.quantity_done + 1
                    if code:
                        if 'serial' in code:
                            product_cls = self.env['stock.production.lot']
                            prd = product_cls.search([
                                ('name', '=', code['serial']),
                                ('product_id', '=', search_product.id)
                            ])

                            if not prd:
                                res = product_cls.create({
                                    'product_id': search_product.id,
                                    'name': code['serial']
                                })
                                self.env['stock.barcode.lot'].create({
                                    'product_id': move_line.product_id.id,
                                    'barcode': self.sh_stock_barcode_mobile,
                                    'move_id': move_line.id,
                                    'lot_id': prd.id or res.id
                                })
            else:
                if search_product:
                    product_cls = self.env['stock.production.lot']
                    lot = False
                    if code is not False and 'serial' in code:
                        prd = product_cls.search([
                            ('name', '=', code['serial']),
                            ('product_id', '=', search_product.id)
                        ])
                        if not prd:
                            res = product_cls.create({
                                'product_id': search_product.id,
                                'name': code['serial']
                            })
                        lot = prd.id or res.id
                    order_line_val = {
                        "name": search_product.name,
                        "product_id": search_product.id,
                        "product_uom_qty": 1,
                        "price_unit": search_product.lst_price,
                        # "quantity_done" : 1,
                        "location_id": self.location_id.id,
                        "location_dest_id": self.location_dest_id.id,
                        'date_expected': str(fields.date.today())
                    }
                    if search_product.uom_id:
                        order_line_val.update({
                            "product_uom": search_product.uom_id.id,
                        })

                    old_lines = self.move_ids_without_package
                    new_order_line = self.move_ids_without_package.create(
                        order_line_val)
                    if new_order_line:
                        self.env['stock.barcode.lot'].create({
                            'product_id': new_order_line.product_id.id,
                            'barcode': self.sh_stock_barcode_mobile,
                            'move_id': new_order_line.id,
                            'lot_id': lot
                        })
                    self.move_ids_without_package = old_lines + new_order_line
                    new_order_line.onchange_product_id()
                    self.sh_stock_barcode_mobile = ''
                    return
                else:
                    msg = 'Scanned Part # / Barcode not exist in any product!'
                    self.sh_stock_barcode_message = msg
                    self.sh_stock_barcode_mobile = ''
                    return
        self.sh_stock_barcode_mobile = ''
        return

    @api.onchange('sh_stock_barcode_mobile_serial')
    def _onchange_sh_stock_barcode_mobile_serial(self):
        self.sh_stock_barcode_message_serial = False
        if self.sh_stock_barcode_mobile_serial in ['', "", False, None]:
            return

        if self:
            search_mls = False
            code = self.parse_barcode(self.sh_stock_barcode_mobile_serial)
            domain = []
            if code:
                if len(code) > 1:
                    if 'serial' in code and 'part' in code:
                        stml = self.move_line_ids_without_package
                        search_mls = stml.filtered(
                            lambda ml: ml.lot_id.name == code['serial'] and
                            ml.product_id.default_code == code['part'])
                        domain = [
                            ('name', '=', code['serial'])]
            else:
                search_mls = self.move_line_ids_without_package.filtered(
                    lambda ml: ml.lot_id.name ==
                    self.sh_stock_barcode_mobile_serial)
                domain = [("name", "=", self.sh_stock_barcode_mobile_serial)]
        search_serial = self.env["stock.production.lot"].search(domain, limit=1)
        if search_mls:
            for move_line in search_mls:
                move_line.qty_done = move_line.qty_done + 1
        else:
            if search_serial:
                move_lines = self.move_line_ids_without_package.filtered(
                    lambda ml: ml.product_id == search_serial.product_id)
                if move_lines:
                    for line in move_lines:
                        line.qty_done = line.qty_done + 1
                        line.lot_id = search_serial.id
                    self.sh_stock_barcode_mobile_serial = ''
                    return
                else:
                    msg = 'Scanned Serial No. does not belong to any item line'
                    self.sh_stock_barcode_message_serial = msg
                    self.sh_stock_barcode_mobile_serial = ''
                    return
            else:
                msg = 'Scanned Serial No. does not exist! Check barcode if ' \
                      'it contains a serial or add manually'
                self.sh_stock_barcode_message_serial = msg
                self.sh_stock_barcode_mobile_serial = ''
                return
            self.sh_stock_barcode_mobile_serial = ''
            return

    @api.multi
    def action_confirm1(self):
        res = super(StockPicking, self).action_confirm()
        sml = self.env['stock.move.line']
        values = self.env['stock.barcode.lot'].search([])
        if res:
            for line in self.move_line_ids:
                vals = values.filtered(lambda x: x.product_id == line.product_id
                                                 and x.move_id == line.move_id)
                # if serial no are unique to products remove the if below and
                # either way it will work since it checks for only one val.
                # Otherwise just let line be single (lot_id = lot_id).However
                # if a product can have different serial nos. The below template
                # can help, though you will break odoo, so food for thought ;-)
                if vals:
                    if len(vals) == 1:
                        line.lot_id = vals.lot_id
                    else:
                        # The following line of code address a concern what if a
                        # product has different serial nos? Odoo seems to say this
                        # is impossible using a checker. But just in case, this code
                        # can be commented out only leave the if statement above.
                        # Also this is bound to fail if you activate show_operations
                        # under stock.picking.type if this happens call me :) its
                        # never personal just a test in mind.
                        line.lot_id = vals[:1].lot_id
                        barcode_objs = vals[1:]
                        for val in barcode_objs:
                            stock_move_line_id = sml.create({
                                'picking_id': line.picking_id.id,
                                'product_id': val.product_id.id,
                                'lot_id': val.lot_id.id,
                                'move_id': val.move_id.id,
                                'product_uom_id': line.product_uom_id.id,
                                'location_id': line.location_id.id,
                                'location_dest_id': line.location_dest_id.id
                            })
                            if stock_move_line_id:
                                write_vals = {
                                    'lot_id': val.lot_id.id,
                                    'revision_number':
                                        val.lot_id.revision_number
                                }
                                stock_move_line_id.write(write_vals)
        return res
