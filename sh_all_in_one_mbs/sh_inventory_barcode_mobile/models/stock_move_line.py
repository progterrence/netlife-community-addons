# -*- coding: utf-8 -*-
from odoo.exceptions import UserError
from odoo.tools.safe_eval import safe_eval
from odoo import api, fields, models, _

class StockMoveLine(models.Model):
    _inherit = 'stock.move.line'

    @api.model
    def create(self, vals):
        barcode_lot = self.env['stock.barcode.lot'].search([
            ('move_id', '=', vals['move_id'])
        ], limit=1)
        if barcode_lot:
            vals['revision_number'] = barcode_lot.lot_id.revision_number
            vals['product_uom_qty'] = 1
        if 'lot_id' in vals and vals['lot_id']:
            vals['product_uom_qty'] = 1
        ret = super(StockMoveLine, self).create(vals)
        return ret
