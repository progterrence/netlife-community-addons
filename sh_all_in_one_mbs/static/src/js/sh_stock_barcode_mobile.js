$(document).ready(function(){
    
	
	
    function decodeOnce(codeReader, selectedDeviceId) {
        codeReader.decodeFromInputVideoDevice(selectedDeviceId, 'video').then((result) => {
        	console.log(result)
        	$('input[name="sh_stock_barcode_mobile"]').val(result.text);    				
	  		$('input[name="sh_stock_barcode_mobile"]').change();

    		//RESET READER
            codeReader.reset();
            // //HIDE VIDEO
    		if($('#js_id_sh_stock_barcode_mobile_vid_div').is(":visible")){
    		    $('#js_id_sh_stock_barcode_mobile_vid_div').hide();
            }

    		// //HIDE STOP BUTTON
    		if($("#js_id_sh_stock_barcode_mobile_reset_btn").is(":visible")){
    		    $("#js_id_sh_stock_barcode_mobile_reset_btn").hide();
            }
    		// //HIDE VIDEO
    		// $('#js_id_sh_stock_barcode_mobile_vid_div').hide();
       		//
    		// //HIDE STOP BUTTON
    		// $("#js_id_sh_stock_barcode_mobile_reset_btn").hide();
    		
    		//RESULT
            document.getElementById('js_id_sh_stock_barcode_mobile_result').textContent = result.text;
    		
            
        }).catch((err) => {
          console.error(err)

        })
      }

      function decodeContinuously(codeReader, selectedDeviceId) {
        codeReader.decodeFromInputVideoDeviceContinuously(selectedDeviceId, 'video', (result, err) => {
          if (result) {
            // properly decoded qr code
            console.log('Found QR code!', result)
        	$('input[name="sh_stock_barcode_mobile"]').val(result.text);    				
    		$('input[name="sh_stock_barcode_mobile"]').change();            
    		
    		//RESULT
            document.getElementById('js_id_sh_stock_barcode_mobile_result').textContent = result.text;
    		
          }

          if (err) {
            // As long as this error belongs into one of the following categories
            // the code reader is going to continue as excepted. Any other error
            // will stop the decoding loop.
            //
            // Excepted Exceptions:
            //
            //  - NotFoundException
            //  - ChecksumException
            //  - FormatException

            if (err instanceof ZXing.NotFoundException) {
            	console.log('No QR code found.')
	          	//EMPTY INPUT
	      		$('input[name="sh_stock_barcode_mobile"]').val('');    				
	      		$('input[name="sh_stock_barcode_mobile"]').change();              
            }

            if (err instanceof ZXing.ChecksumException) {
              console.log('A code was found, but it\'s read value was not valid.')
            }

            if (err instanceof ZXing.FormatException) {
              console.log('A code was found, but it was in a invalid format.')
            }
          }
        })
      }	
	
	
      
	
	
	
	//HIDE STOP BUTTON (SAFETY IN XML WE ALSO DO AND HERE ALSO.)
	if($("#js_id_sh_stock_barcode_mobile_reset_btn").is(":visible")){
	    console.log("HERE!");
	    $("#js_id_sh_stock_barcode_mobile_reset_btn").hide();
	}

    let selectedDeviceId;

    
    
    const codeReader = new ZXing.BrowserMultiFormatReader()
    //const codeReader = new ZXing.BrowserBarcodeReader()    
    
    console.log('ZXing code reader initialized');
    var count = 0;
    codeReader.getVideoInputDevices()
    .then(function(result) {
    	//THEN METHOD START HERE
    	//const sourceSelect = $("#js_id_sh_stock_barcode_mobile_cam_select");
    	const sourceSelect = document.getElementById('js_id_sh_stock_barcode_mobile_cam_select');
		
    	$('input[name="sh_stock_barcode_mobile"]').val('');    				
		$('input[name="sh_stock_barcode_mobile"]').change();
		
        _.each(result, function(item) {
            //self._add_filter(item.partner_id[0], item.partner_id[1], !active_partner, true);
            const sourceOption = document.createElement('option')
            sourceOption.text = item.label
            sourceOption.value = item.deviceId
            sourceSelect.appendChild(sourceOption)
            
        });
                
        //CUSTOM EVENT HANDLER START HERE
        
 
        /*
         * =============================
         * ONCHANGE SELECT CAMERA
         * =============================
         */        
    	$(document).on('change', '#js_id_sh_stock_barcode_mobile_cam_select', function() {
    		  // Does some stuff and logs the event to the console
    		selectedDeviceId = sourceSelect.value;    
            
    	});
        	
        
        
        
        /*
         * ========================
         * WHEN CLICK START BUTTON.
         * ========================
         */
        $(document).on("click","#js_id_sh_stock_barcode_mobile_start_btn",function(event) {
            $('.o_form_button_save').prop('disabled', false);
        	//EMPTY INPUT
    		$('input[name="sh_stock_barcode_mobile"]').val('');    				
    		$('input[name="sh_stock_barcode_mobile"]').change();
    		if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
                || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {
    		    //SHOW VIDEO
                if($('#js_id_sh_stock_barcode_mobile_vid_div').is(':hidden'))
    		    $('#js_id_sh_stock_barcode_mobile_vid_div').show();

    		    //SHOW STOP BUTTON
                if($('#js_id_sh_stock_barcode_mobile_reset_btn').is(':hidden'))
    		    $("#js_id_sh_stock_barcode_mobile_reset_btn").show();

    		    //CALL METHOD
			    //CONTINUOUS SCAN OR NOT.
			    if( $('span[name="sh_stock_bm_is_cont_scan"]').text() == 'True' ){
			        decodeContinuously(codeReader, selectedDeviceId);
			    }else{
			        decodeOnce(codeReader, selectedDeviceId);
			    }

    		}
    		else {
    		    return alert("Sorry current device is not a mobile device," +
                    " services available only to mobile devices.");
            }
        });
        
           
        
        /*
         * =============================
         * WHEN CLICK STOP/RESET BUTTON.
         * =============================
         */
        $(document).on("click","#js_id_sh_stock_barcode_mobile_reset_btn",function() {
            console.log('STOP CAMERA');
            $('.o_form_button_save').prop('disabled', false);
            document.getElementById('js_id_sh_stock_barcode_mobile_result').textContent = '';
    		
            //EMPTY VALUE
            $('input[name="sh_stock_barcode_mobile"]').val('');    				
    		$('input[name="sh_stock_barcode_mobile"]').change();
    		// stop streams
    		codeReader.stopStreams()
    		//RESET READER
            codeReader.reset();

    		 // //HIDE VIDEO
    		if($('#js_id_sh_stock_barcode_mobile_vid_div').is(":visible")){
    		    $('#js_id_sh_stock_barcode_mobile_vid_div').hide();
            }

    		// // //HIDE STOP BUTTON *BUG*
    		// if($("#js_id_sh_stock_barcode_mobile_reset_btn").is(":visible")){
    		//     $("#js_id_sh_stock_barcode_mobile_reset_btn").hide();
            // }
            
            
        });

        return;
                
        
    // CUSTOM ENENT HANDLER ENDS HERE
        
    // THEN METHOD ENDS HERE
    }).catch(function (reason) { 
    	
  	  console.log("Error ==>" + reason);
  });
    
    
    
});


	




